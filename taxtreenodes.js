/**
 * TaxTreeNodes collapse/expand
 *
 */

Drupal.taxtreenodestoggle = function () {
  // attach the onclick handlers to show comments
	$('a.taxtreenodestoggle').click(function()
		{
			$(this).next('ul').toggle();
			$(this).next('ul').next('ul').toggle();
			return false;
		}
	);

  // hide all comments on page display
  $('a.taxtreenodestoggle').each(function() {
    $(this).next('ul').toggle();
    $(this).next('ul').next('ul').toggle();
  });
}

// Global killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.taxtreenodestoggle);
}

