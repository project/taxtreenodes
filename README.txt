
Tax Tree Nodes
--------------

Tax Tree Nodes shows a taxonomy including all terms with at least one node attached to it as well as a link to the node.

The admin settings allows you to:
- Show node counts by categories.
  You can exclude certain taxonomies. 
- Make the tree collapsible using jQuery.
  You can exclude certain taxonomies. 
- Exclude certain node types (sponsored by marmaladesoul.com)  

You can theme the output using the following functions:
- theme_taxtreenodes_term 
- theme_taxtreenodes_nodes 
- theme_taxtreenodes_node 

How to use
----------

Settings can be found at http://www.example.com/admin/settings/taxtreenodes

To view taxonomy with id 1 as a tree goto http://www.example.com/taxtreenodes/1

